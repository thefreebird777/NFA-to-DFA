import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Ian on 3/19/2017.
 */
public class NFA {
    public static int states; // number of states
    public static int startingState; //starting state
    public static ArrayList<Integer> acceptingStates = new ArrayList<Integer>(); //arraylist for accepting states
    public static ArrayList<String> characters = new ArrayList<String>(); // arraylist of acceptable characters
    public static ArrayList<String> transitions = new ArrayList<String>(); //arraylist of all transitions, before being put into sets
    public static ArrayList<String> input = new ArrayList<String>();

    public int stateNumber = 0;
    public String newState;
    public ArrayList<Integer> currentState = new ArrayList<Integer>();
    public Map<String, ArrayList> transMap = new HashMap<String, ArrayList>(); //maps character to it's transition states
    public ArrayList<Integer> transStates; //arraylist of transition states
    public ArrayList<Map> mapAL = new ArrayList<Map>(); // arraylist of the map for each state
    public ArrayList<String> newStates = new ArrayList<String>();
    public ArrayList<Integer> closureAL = new ArrayList<Integer>();
    public ArrayList<String> unCheckedStates = new ArrayList<String>();
    public ArrayList<String> checkedStates = new ArrayList<String>();
    public Map<String, Integer> stateMap = new HashMap<String, Integer>();
    public ArrayList<Integer> dfaAcceptingStates = new ArrayList<Integer>();
    public ArrayList<Integer> dfaList;
    public List<List<Integer>> listOfLists = new ArrayList<List<Integer>>();
    public Map<String, Integer> listMap = new HashMap<String, Integer>();
    public ArrayList<String> acceptedStrings = new ArrayList<String>();


    public void printNFA() {
        System.out.print("Sigma: ");
        for (int i = 0; i < characters.size(); i++) {
            System.out.print(characters.get(i) + " ");
        }
        System.out.print("\n-------\n");
        int k = 0;
        for (int i = 0; i < states; i++) {
            System.out.print(i + ": ");
            for (int j = 0; j < characters.size(); j++) {
                System.out.print("(" + characters.get(j) + "," + transitions.get(k) + ") ");
                k++;
            }
            System.out.print("( ," + transitions.get(k) + ")\n");
            k++;
        }
        System.out.print("-------\n");
        System.out.print(startingState + ": Initial State\n");
        if (acceptingStates.size() == 1) {
            System.out.print(acceptingStates.get(0));
        } else {
            int count = 0;
            for (int i = 0; i < acceptingStates.size() - 1; i++) {
                System.out.print(acceptingStates.get(i) + ", ");
                count++;
            }
            System.out.print(acceptingStates.get(count));
        }
        System.out.print(": Accepting States\n");
    }

    public void printDFA() {
        System.out.print("\nTo DFA:\n");
        System.out.print("Sigma: ");
        for (int i = 0; i < characters.size(); i++) {
            System.out.print(characters.get(i) + " ");
        }
        System.out.print("\n-------------\n");
        for (int i = 0; i < newStates.size(); i++) {
            System.out.print(stateMap.get(newStates.get(i)) + ":     ");
            for (int j = 0; j < characters.size(); j++) {
                System.out.print(listOfLists.get(i).get(listMap.get(characters.get(j))) + " ");
            }
            System.out.print("\n");
        }
        System.out.print("-------------\n");
        System.out.print(stateMap.get(newStates.get(0)) + ": Initial State\n");
        if (acceptingStates.size() == 1) {
            System.out.print(dfaAcceptingStates.get(0));
        } else {
            int count = 0;
            for (int i = 0; i < dfaAcceptingStates.size() - 1; i++) {
                System.out.print(dfaAcceptingStates.get(i) + ", ");
                count++;
            }
            System.out.print(dfaAcceptingStates.get(count));
        }
        System.out.print(": Accepting States\n");
    }

    public void closure(int state) {
        for (int j = 0; j < closureAL.size(); j++) {
            if (state == closureAL.get(j)) {
                return;
            }
        }
        closureAL.add(state);
        transMap = mapAL.get(state);
        if (transMap.get("LAMBDA") != null) { // check if LAMBDA is key in the map
            ArrayList<Integer> tempAL = transMap.get("LAMBDA"); //sets a temp arraylist of states that are mapped to LAMBDA
            if (tempAL.get(0) != -1) {
                for (int k = 0; k < tempAL.size(); k++) { // for amount of states mapped to LAMBDA
                    closure(tempAL.get(k));
                }
            }
        }
        String s = "";
        for (int j = 0; j < closureAL.size(); j++) {
            s += closureAL.get(j) + " ";
        }
        newState = s;
    }

    public String sort(String s) {
        StringTokenizer st = new StringTokenizer(s);
        ArrayList<Integer> tempAL = new ArrayList<Integer>();
        while (st.hasMoreElements()) {
            String temp = st.nextToken(" ");
            tempAL.add(Integer.parseInt(temp));
        }
        Collections.sort(tempAL);
        s = "";
        for (int k = 0; k < tempAL.size(); k++) {
            s += tempAL.get(k) + " ";
        }
        return s;
    }

    public void mapChar() {
        for (int i = 0; i < characters.size(); i++) {
            listMap.put(characters.get(i), i);
        }
    }

    public ArrayList<Integer> transition(ArrayList<Integer> state, String c) {
        ArrayList<Integer> transAL = new ArrayList<Integer>();
        for (int i = 0; i < state.size(); i++) {
            transMap = mapAL.get(state.get(i));
            if (transMap.get(c) != null) { //check for mapping with character as key
                ArrayList<Integer> tempAL = transMap.get(c); // sets tempAL to arraylist of states that was mapped to character
                if (tempAL.get(0) != -1) { //checks that state or states exist
                    for (int j = 0; j < tempAL.size(); j++) { //loop for number of states mapped to character
                        transAL.add(tempAL.get(j)); // go to next state that is linked from the current state
                    }
                }
            }
        }
        return transAL;
    }

    public void toDFA() {
        closure(startingState);
        closureAL.clear();
        newState = sort(newState);
        newStates.add(newState);
        checkedStates.add(newState);
        stateMap.put(newStates.get(0), stateNumber);
        StringTokenizer st = new StringTokenizer(newState);
        while (st.hasMoreElements()) {
            String temp = st.nextToken(" ");
            currentState.add(Integer.parseInt(temp));
        }
        while (currentState != null) {
            dfaList = new ArrayList<Integer>();
            for (int i = 0; i < characters.size(); i++) { // for amount of characters
                String s = "";
                ArrayList<Integer> temp = transition(currentState, characters.get(i)); //gets transition states
                for (int j = 0; j < temp.size(); j++) { //for num of transition states
                    closure(temp.get(j)); //find closure
                    closureAL.clear();
                    st = new StringTokenizer(newState);
                    ArrayList<String> tempAL = new ArrayList<String>();
                    while (st.hasMoreElements()) {
                        String temp2 = st.nextToken(" ");
                        tempAL.add(temp2);
                    }
                    boolean isIn = false;
                    for (int k = 0; k < tempAL.size(); k++) {
                        for (int y = 0; y < s.length(); y++) {
                            if (tempAL.get(k).charAt(0) == s.charAt(y)) {
                                isIn = true;
                            }
                        }
                        if (isIn == false) {
                            s += tempAL.get(k) + " ";
                        }
                    }
                }
                s = sort(s);
                boolean isIn = false;
                for (int j = 0; j < newStates.size(); j++) { //check if state already exists
                    if (s.equals(newStates.get(j))) {
                        isIn = true;
                    }
                }
                if (isIn == false) {
                    newStates.add(s);
                    stateNumber++;
                    stateMap.put(newStates.get(stateNumber), stateNumber);
                }
                isIn = false;
                for (int j = 0; j < checkedStates.size(); j++) { //check if state was already Checked
                    if (s.equals(checkedStates.get(j))) {
                        isIn = true;
                    }
                }
                for (int j = 0; j < unCheckedStates.size(); j++) { //check if state is in the check queue
                    if (s.equals(unCheckedStates.get(j))) {
                        isIn = true;
                    }
                }
                if (isIn == false) {
                    unCheckedStates.add(s);
                }
                String xyz = "";
                for (int z = 0; z < currentState.size(); z++) {
                    xyz += currentState.get(z) + " ";
                }

                dfaList.add(stateMap.get(s));
                listOfLists.add(stateMap.get(xyz), dfaList);
            }
            currentState.clear();
            if (!unCheckedStates.isEmpty()) {
                st = new StringTokenizer(unCheckedStates.get(0));
                while (st.hasMoreElements()) {
                    String temp = st.nextToken(" ");
                    currentState.add(Integer.parseInt(temp));
                }
                checkedStates.add(unCheckedStates.get(0));

                unCheckedStates.remove(0);
            }
            //set accepting states
            if (unCheckedStates.isEmpty() && currentState.isEmpty()) {
                for (int i = 0; i < acceptingStates.size(); i++) {
                    for (int j = 0; j < newStates.size(); j++) {
                        st = new StringTokenizer(newStates.get(j));
                        while (st.hasMoreElements()) {
                            String temp = st.nextToken(" ");
                            int k = Integer.parseInt(temp);
                            if (acceptingStates.get(i) == k) {
                                boolean isIn = false;
                                for (int w = 0; w < dfaAcceptingStates.size(); w++) {
                                    if (stateMap.get(newStates.get(j)) == dfaAcceptingStates.get(w)) {
                                        isIn = true;
                                    }
                                }
                                if (!isIn) {
                                    dfaAcceptingStates.add(stateMap.get(newStates.get(j)));
                                    break;
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    public void toHashMap() {
        int k = 0;
        for (int i = 0; i < states; i++) {
            transMap = new HashMap<String, ArrayList>();
            for (int j = 0; j <= characters.size(); j++) {
                transStates = new ArrayList<Integer>();
                String temp = transitions.get(k);
                int w;
                StringTokenizer st = new StringTokenizer(temp);
                while (st.hasMoreTokens()) {
                    try {
                        String temp2 = st.nextToken("{,}");
                        w = Integer.parseInt(temp2);
                    } catch (NoSuchElementException nse) {
                        w = -1;
                    }
                    transStates.add(w);
                }
                if (j < characters.size()) {
                    transMap.put(characters.get(j), transStates);
                } else {
                    transMap.put("LAMBDA", transStates);
                }
                k++;
            }
            mapAL.add(i, transMap);
        }
    }

    public void inputString() {
        for (int i = 0; i < input.size(); i++) { // for amount of input
            String s = "";
            int place = startingState;
            for (int j = 0; j < input.get(i).length(); j++) { //for length of input
                for (int k = 0; k < characters.size(); k++) { //for available characters
                    if (String.valueOf(input.get(i).charAt(j)).equals(characters.get(k))) {
                        if (listOfLists.get(place).get(listMap.get(characters.get(k))) != '\0') {
                            place = listOfLists.get(place).get(listMap.get(characters.get(k)));
                            s += characters.get(k);
                        }
                    }
                }
            }
            if (input.get(i).equals(s)) {
                for (int j = 0; j < dfaAcceptingStates.size(); j++) {
                    if (place == dfaAcceptingStates.get(j)) {
                        acceptedStrings.add(s);
                        break;
                    }
                }

            }
        }
        System.out.println("\nThe following strings are accepted:");
        for (int i = 0; i < acceptedStrings.size(); i++) {
            System.out.println(acceptedStrings.get(i));
        }
    }

    public static void main(String args[]) {
        NFA nfa = new NFA();
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(args[0]));
            String line;
            line = br.readLine();
            if (line != null) {
                states = Integer.parseInt(line);
            }
            line = br.readLine();
            if (line != null) {
                StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreElements()) {
                    characters.add(st.nextToken("\t "));
                }
            }
            if (states != 0) {
                for (int i = 0; i < states; i++) {
                    line = br.readLine();
                    if (line != null) {
                        StringTokenizer st = new StringTokenizer(line);
                        st.nextToken("\t ");
                        while (st.hasMoreElements()) {

                            transitions.add(st.nextToken("\t "));
                        }
                    }
                }
            }
            line = br.readLine();
            if (line != null) {
                startingState = Integer.parseInt(line);
            }
            line = br.readLine();
            if (line != null) {
                String temp;
                StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreElements()) {
                    temp = st.nextToken("\t{}, ");
                    acceptingStates.add(Integer.parseInt(temp));
                }
            }
            nfa.printNFA();
            nfa.toHashMap();
            nfa.mapChar();
            nfa.toDFA();
            nfa.printDFA();
        } catch (FileNotFoundException e) {
            System.out.println("Could not find file.");
        } catch (IOException e) {
            System.out.println("Could not read file.");
        }
        try {
            br = new BufferedReader(new FileReader(args[1]));
            String line;
            line = br.readLine();
            while (line != null) {
                input.add(line);
                line = br.readLine();
            }
            nfa.inputString();
        } catch (FileNotFoundException e) {
            System.out.println("Could not find file.");
        } catch (IOException e) {
            System.out.println("Could not read file.");
        }

    }
}
